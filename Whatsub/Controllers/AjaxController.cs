﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TMDbLib.Objects.General;
using Microsoft.AspNet.Identity;
using Whatsub.Models;
using System.Text.RegularExpressions;

namespace Whatsub.Controllers
{
    public class AjaxController : ParentController
    {
        /*
         * Retrieves suggestions for the dropdown
         * in search bars for Subtitles.
         */
        public JsonResult SubtitleSuggestion(string query)
        {
            var Model = new SuggestionModel<string>();

            Model.query = query;
            Model.suggestions = (from p in _ProfileRepo.GetAll()
                                 where p.Subtitles.Count() != 0
                                 && p.Title.ToLower().Contains(query.ToLower())
                                 select p.Title).ToArray();

            return Json(Model, JsonRequestBehavior.AllowGet);
        }

        /*
         * Retrieves suggestions for the dropdown
         * in search bars for Requests
         */
        public JsonResult RequestSuggestion(string query)
        {
            var Model = new SuggestionModel<string>();

            Model.query = query;
            Model.suggestions = (from p in _ProfileRepo.GetAll()
                                 where p.Requests.Count() != 0
                                 && p.Title.ToLower().Contains(query.ToLower())
                                 select p.Title).ToArray();

            return Json(Model, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult CompleteStatus(CompleteStatusViewModel Complete)
        {
            Subtitle SubtitleFound = (from s in _SubtitleRepo.GetAll()
                                      where s.ID == Complete.SubtitleID
                                      select s).SingleOrDefault();

            if (SubtitleFound != null)
            {
                //Update Subtitle
                SubtitleFound.Complete = Complete.CompleteStatus;
                _SubtitleRepo.Save();
            }
            return Json(new { Message = "Success" }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult SaveLine(EditSubtitleSaveModel Line)
        {
            Line EditLine = (from L in _LineRepo.GetAll()
                             where L.ID == Line.LineID
                             select L).SingleOrDefault();

            EditLine.Line1 = Line.Line1;
            EditLine.Line2 = Line.Line2;
            EditLine.TimeStart = TimeSpan.Parse(Line.TimeStart);
            EditLine.TimeEnd = TimeSpan.Parse(Line.TimeEnd);

            _LineRepo.Save();

            return Json(new { Message = "Success" }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult DeleteLine(EditSubtitleSaveModel Line)
        {
            // Find line to delete and delete it from DB
            Line DeleteLine = (from L in _LineRepo.GetAll()
                             where L.ID == Line.LineID
                             select L).SingleOrDefault();
            if (DeleteLine != null)
            {
                // Line found
                _LineRepo.Delete(DeleteLine);
                _LineRepo.Save();
            }
            return Json(new { Message = "Success" }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult AddLine(AddSubtitleSaveModel Line)
        {
                //Make New line
                Line NewLine = new Line()
                {
                    //ApplicationUserID = ? TODO
                    Date = DateTime.Now,
                    SubtitleID = Line.SubtitleID,
                    TimeStart = TimeSpan.Parse(Line.TimeStart),
                    TimeEnd = TimeSpan.Parse(Line.TimeEnd),
                    Line1 = Line.Line1,
                    Line2 = Line.Line2,
                };
                _LineRepo.Add(NewLine);
                _LineRepo.Save();

                return Json(new { Message = "Success" }, JsonRequestBehavior.AllowGet);


            
        }

        [HttpPost]
        public JsonResult Upvote(int? RequestID, int? UpvoteID)
        {
            Upvote Upvote;
            if (!UpvoteID.HasValue)
            {
                Upvote = new Upvote { SubtitleRequestID = RequestID.Value };
                _UpvoteRepo.Add(Upvote);
            }
            else
            {
                Upvote = (from U in _UpvoteRepo.GetAll()
                                    where U.ID == UpvoteID.Value
                                    select U).SingleOrDefault();
                _UpvoteRepo.Delete(Upvote);
            }
            _UpvoteRepo.Save();
            int count = (from U in _UpvoteRepo.GetAll()
                         where U.SubtitleRequestID == RequestID.Value
                         select U).Count();

            int ID = Upvote.ID;

            return Json(new { id = ID, Upcount = count }, JsonRequestBehavior.AllowGet);
        }
    }
}