﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Whatsub.Repositories;
using Whatsub.Models;
using TMDbLib.Client;
using TMDbLib.Objects.Movies;
using TMDbLib.Objects.General;
using System.Text;
using System.IO;
using System.Text.RegularExpressions;
using Microsoft.AspNet.Identity;
using System.Globalization;


namespace Whatsub.Controllers
{
    public class SubtitleController : ParentController
    {
        public SubtitleController() : base() { }
        public SubtitleController(IRepository<SubtitleProfile> ProfileRepo, IRepository<Subtitle> SubtitleRepo = null) : base(ProfileRepo, SubtitleRepo) { }

        /* 
         * Takes care of displaying the list of SubtitleProfiles 
         * on current page
         */
        public ActionResult Index(int? Page)
        {
            // If Page has no value we have to set it to 1 to represent page 1
            if (!Page.HasValue)
            {
                Page = 1;
            }

            // Fetch all Profiles from the Database that have one or more subtitles
            var Profiles = (from x in _ProfileRepo.GetAll()
                            where x.Subtitles.Count() != 0
                            orderby x.ID descending
                            select x).Skip((Page.Value - 1) * 10).Take(10).ToList();

            PageModel<SubtitleSearchViewModel> model = new PageModel<SubtitleSearchViewModel>();
            model.Profiles = new List<SubtitleSearchViewModel>();

            // Total profiles
            model.Total = (from x in _ProfileRepo.GetAll()
                           where x.Subtitles.Count() != 0
                           select x).Count();
            // Calculate how many pages there are in total
            model.Pages = (int)Math.Ceiling((double)model.Total / 10);
            // Put the current page into the model
            model.CurrentPage = Page.Value;

            foreach (var P in Profiles)
            {
                // Check if there is an ID for The Movie DB
                if (P.MovieDBID.HasValue)
                {
                    // Current profile is a movie
                    if (P.TypeID == 1)
                    {
                        var movie = _TMDb.GetMovie(P.MovieDBID.Value);
                        model.Profiles.Add(new SubtitleSearchViewModel { ID = P.Subtitles.First().ID, Title = P.Title, Year = P.Year, PosterPath = movie.PosterPath });
                    }
                    // Current profile is a TV Show
                    else
                    {
                        var TvShow = _TMDb.GetTvShow(P.MovieDBID.Value);
                        model.Profiles.Add(new SubtitleSearchViewModel { ID = P.Subtitles.First().ID, Title = P.Title, Year = P.Year, PosterPath = TvShow.PosterPath });
                    }
                }
                else
                {
                    model.Profiles.Add(new SubtitleSearchViewModel { ID = P.Subtitles.First().ID, Title = P.Title, Year = P.Year });
                }
            }

            return View(model);
        }

        /* 
         * Takes care of subtitle search
         */
        /*
         * Search query finds 10 results that contain the word
         * And lists them on the site, finds the corresponding
         * movie poster and info
        */
        public ActionResult Search(string SearchQuery)
        {
            //Checks for empty input
            if (string.IsNullOrEmpty(SearchQuery))
            {
                ModelState.AddModelError("", "");
            }
            //Search DB for subtitle with title that contains the input
            var Profiles = (from x in _ProfileRepo.GetAll()
                            where x.Subtitles.Count() != 0 && x.Title.ToLower().Contains(SearchQuery.ToLower())
                            orderby x.Title
                            select x).Take(10).ToList();

            PageModel<SubtitleSearchViewModel> model = new PageModel<SubtitleSearchViewModel>();
            model.Profiles = new List<SubtitleSearchViewModel>();

            // Search only shows first 10
            model.CurrentPage = 0;
            model.Pages = 0;
            model.Total = 0;

            foreach (var P in Profiles)
            {
                // Check if there is an ID for The Movie DB
                if (P.MovieDBID.HasValue)
                {
                    // Current profile is a movie
                    if (P.TypeID == 1)
                    {
                        Movie movie = _TMDb.GetMovie(P.MovieDBID.Value);
                        model.Profiles.Add(new SubtitleSearchViewModel { ID = P.Subtitles.First().ID, Title = P.Title, Year = P.Year, PosterPath = movie.PosterPath });
                    }
                    // Current profile is a TV Show
                    else
                    {
                        var TvShow = _TMDb.GetTvShow(P.MovieDBID.Value);
                        model.Profiles.Add(new SubtitleSearchViewModel { ID = P.Subtitles.First().ID, Title = P.Title, Year = P.Year, PosterPath = TvShow.PosterPath });
                    }
                }
                else
                {
                    model.Profiles.Add(new SubtitleSearchViewModel { ID = P.Subtitles.First().ID, Title = P.Title, Year = P.Year });
                }

            }

            // Redirect to details page about movie if there is a single result
            if (model.Profiles.Count() == 1)
            {
                return RedirectToAction("View", new { id = model.Profiles.First().ID });
            }
            return View("Index", model);
        }

        /* 
         * Takes care of displaying the corresponding 
         * Subtitle profile page.
         * Gathers info about the page and returns it
         * to the view model.
         */
        public ActionResult View(int? id)
        {
            if (id.HasValue)
            {
                //Finds subtitle from input id and fill the ViewModel
                SubtitleProfilePageViewModel model = (from S in _SubtitleRepo.GetAll()
                                                      where S.ID == id.Value
                                                      select new SubtitleProfilePageViewModel { 
                                                                ID = S.ID,
                                                                ProfileID = S.SubtitleProfileID,
                                                                Title = S.SubtitleProfile.Title,
                                                                Year = S.SubtitleProfile.Year,
                                                                MovieDBID = S.SubtitleProfile.MovieDBID,
                                                                YoutubeUrl = S.SubtitleProfile.YouTubeURL,
                                                                Complete = S.Complete,
                                                                TypeID = S.SubtitleProfile.TypeID
                                                      }).SingleOrDefault();
                //Subtitle found
                if (model != null)
                {
                    //Get Language for subtitle
                    var Languages = (from L in _LanguageRepo.GetAll()
                                    join S in _SubtitleRepo.GetAll()
                                        on L.ID equals S.LanguageID
                                    where S.SubtitleProfileID == model.ProfileID
                                    select new
                                    {
                                        Text = L.LanguageName,
                                        Value = S.ID
                                    }).ToList();

                    //Language select list for form
                    var LanguageList = new List<SelectListItem>();
                    foreach ( var Language in Languages )
                    {
                        SelectListItem Item = new SelectListItem
                        {
                            Text = Language.Text,
                            Value = Language.Value.ToString(),
                            Selected = false
                        };

                        if (Language.Value == id.Value)
                        {
                            Item.Selected = true;
                        }

                        LanguageList.Add(Item);
                    }
                    ViewBag.Languages = LanguageList;

                    // Check if there is an ID for The Movie DB
                    if (model.MovieDBID.HasValue)
                    {
                        if (model.TypeID == 1)
                        {
                            // Get details from TheMovieDB
                            Movie movie = _TMDb.GetMovie(model.MovieDBID.Value);
                            // Populate the model object
                            model.PosterPath = movie.PosterPath;
                            model.Overview = movie.Overview;
                        }
                        else
                        {
                            //Get details from TheMovieDB
                            var TvShow = _TMDb.GetTvShow(model.MovieDBID.Value);
                            //Populate the model object
                            model.PosterPath = TvShow.PosterPath;
                            model.Overview = TvShow.Overview;
                        }
                    }

                    return View(model); // Success
                }
                else
                {
                    return View("SubtitleNotFound"); // ID not found
                }
            }

            return View("Error"); // No ID
        }


        /* 
         * Takes care of displaying the edit page for each
         * language. Receives info about the pagenumber and returns
         * the corresponding lines to the view model. 
         */
        [Authorize]
        public ActionResult Edit(int? id, int? PageNumber)
        { 
            if (id.HasValue)
            {
                //Finds subtitle from input id and fill the ViewModel
                var model = (from S in _SubtitleRepo.GetAll()
                             where S.ID == id.Value
                             select new EditSubtitlePageViewModel
                             {
                                 Complete = S.Complete,
                                 ID = S.ID,
                                 Title = S.SubtitleProfile.Title,
                                 Year = S.SubtitleProfile.Year,
                                 Lines = (from L in S.Lines
                                          orderby L.TimeStartTicks
                                          select L).ToList(),
                                 Comments = S.Comments,
                                 SubtitleID = S.ID,
                                 SubtitleLanguageID = S.LanguageID
                             }).SingleOrDefault();

                //Finding Language from LanguageID
                var Language = (from l in _LanguageRepo.GetAll()
                                where l.ID == model.SubtitleLanguageID
                                select l).Single();
                model.SubtitleLanguage = Language.LanguageName;

                //SelectListItem for the status of the subtitle
                List<SelectListItem> CompleteStatus = new List<SelectListItem>();
                CompleteStatus.Add(new SelectListItem { Text = "Complete", Value = "True" });
                CompleteStatus.Add(new SelectListItem { Text = "Not Complete", Value = "False" });

                //Make the current status selected
                foreach (SelectListItem c in CompleteStatus)
                {
                    if (model.Complete && (c.Value == "True"))
                    {
                        c.Selected = true;
                    }
                    if (!model.Complete && (c.Value == "False"))
                    {
                        c.Selected = true;
                    }
                }
                ViewBag.CompleteStatus = CompleteStatus;

                // If Page has no value we have to set it to 1 to represent page 1
                if(!PageNumber.HasValue)
                {
                    PageNumber = 1;
                }
                /*
                 *  Pages holds the number of pages needed for the lines
                 *  Pagenumber is the value of the page the user is currently on
                 *  Lines are the lines represented on that page.
                 */
                model.Pages = (int)Math.Ceiling((double)model.Lines.Count / 10);
                model.PageNumber = PageNumber.Value;
                model.Lines = model.Lines.Skip((PageNumber.Value - 1) * 10).Take(10).ToList();
                if (model != null)
                {
                    return View(model);
                }
            }

            return View("Error");
        }

        /* 
         * Used to reload the edit table instead of 
         * issuing a full http request for the entire site.
         * Receives info about the pagenumber and returns
         * the lines to the view model.
         */

        [Authorize]
        public PartialViewResult EditTablePartialView(int id, int? PageNumber)
        {
            var model = (from S in _SubtitleRepo.GetAll()
                         where S.ID == id
                         select new EditSubtitlePageViewModel
                         {
                             ID = S.ID,
                             Title = S.SubtitleProfile.Title,
                             Year = S.SubtitleProfile.Year,
                             Lines = (from L in S.Lines    
                                      orderby L.TimeStartTicks
                                      select L).ToList(),
                             Comments = S.Comments,
                             SubtitleID = S.ID
                         }).SingleOrDefault();
            if (!PageNumber.HasValue)
            {
                PageNumber = 1;
            }
            model.Pages = (int)Math.Ceiling((double)model.Lines.Count / 10);
            model.PageNumber = PageNumber.Value;
            model.Lines = model.Lines.Skip((PageNumber.Value - 1) * 10).Take(10).ToList();

            return PartialView("EditTablePartial",model);
        }

        /*
         * Adds a comment to the database
         * and returns all the comments again.
         */

        [HttpPost]
        public ActionResult AddComment(Comment Comment)
        {
            Comment.ApplicationUserID = User.Identity.GetUserId();
            Comment.Date = DateTime.Now;
            _CommentRepo.Add(Comment);
            _CommentRepo.Save();
            return GetComments(Comment.SubtitleID);
        }

        /*
         * Returns all comments to the corresponding
         * language id in descending order, i.e. newest first.
         */

        public ActionResult GetComments(int? id)
        {
            var newResult = (from C in _CommentRepo.GetAll()
                             where C.SubtitleID == id.Value
                            orderby C.Date descending
                            select new
                            {
                                CommentDate = C.Date,
                                SubtitleID = C.SubtitleID,
                                CommentText = C.CommentText,
                                Username = C.ApplicationUser.UserName
                            }).Take(5);

            return Json(newResult, JsonRequestBehavior.AllowGet);
        }

        /*
         * Returns the user to the add subtitle page
         * Where he can fill out the form to submit a subtitle
         */
        [HttpGet]
        [Authorize]
        public ActionResult Add(AddSubtitleViewModel model)
        {
            //Make a select list for the type of subtitle
            List<SelectListItem> types = new List<SelectListItem>();
            types.Add(new SelectListItem { Text = "Movie", Value = "1" });
            types.Add(new SelectListItem { Text = "TV-Series", Value = "2" });
            ViewBag.TypeID = types;

            //Gets the languages from DB
            List<Language> LanguageFromDB = (from l in _LanguageRepo.GetAll()
                                             select l).ToList();

            //Makes a select list for languages
            List<SelectListItem> Language = new List<SelectListItem>();
            foreach(Language l in LanguageFromDB){
                SelectListItem NewSelectListItem = new SelectListItem { 
                    Text = l.LanguageName, 
                    Value = l.ID.ToString()
                };
                Language.Add(NewSelectListItem);
            }
            ViewBag.LanguageID = Language;

            if (ModelState.IsValid)
            { 
                return View(model);
            }
            else
            {
                return View(new AddSubtitleViewModel());
            }
        }

        /*
         * Gets the filled out form from the user
         * Possible outcomes:
         * Adds subtitle to DB
         * Redirects user to subtitle edit page
         * Redirects user to error page
         * 
         */
        [HttpPost]
        [Authorize]
        [ValidateAntiForgeryToken]
        public ActionResult Create(AddSubtitleViewModel model)
        {
            //Checks if everything is valid
            if (ModelState.IsValid)
            {
                if (model.SrtUpload != null)
                {
                    //Checking fileExtension
                    string FileExtension = Path.GetExtension(model.SrtUpload.FileName);
                    if (FileExtension != ".srt")
                    {
                        return View("AddSubtitleError");
                    }
                }
                
                //Gets SubtitleProfile with model.Title
                var OldSubtitleProfile = GetSubtitleProfileFromTitle(model.Title);

                //if SubtitleProfile exists in DB
                if (OldSubtitleProfile != null) 
                {
                    //Gets SubtitleProfile with model.Title
                    var OldSubtitle = GetSubtitleFromSubtitleProfileIDAndLanguageID(OldSubtitleProfile.ID, model.LanguageID);

                    //if Subtitle with the language exists in DB
                    if(OldSubtitle != null){
                        //Send user to Edit Page for Subtitle
                         return RedirectToAction("Edit", new { id = OldSubtitle.ID });
                    }
                    //if Subtitle with the language does not exists in DB
                    else{
                        //Check for request
                        var FindRequest = GetRequestFromSubtitleProfileIDAndLanguageID(OldSubtitleProfile.ID, model.LanguageID);
                        //delete request if it exists
                        if (FindRequest != null)
                        {
                            _RequestRepo.Delete(FindRequest);
                        }
                        //Create new Subtitle with new Language
                        var NewSubtitle = new Subtitle
                        {
                            Complete = false,
                            SubtitleProfileID = OldSubtitleProfile.ID,
                            SubtitleProfile = OldSubtitleProfile,
                            LanguageID = model.LanguageID
                        };
                        //Adding Subtitle to SubtitleProfile in DB
                        OldSubtitleProfile.Subtitles.Add(NewSubtitle);

                        //SRT file has been uploaded
                        if (model.SrtUpload != null)
                        {
                            //Upload Srt file
                            if (!UploadSrt(NewSubtitle, model.SrtUpload))
                            {
                                //if failed
                                return View("AddSubtitleError");
                            }
                            //Should go to edit page
                            return RedirectToAction("Edit", new { id = NewSubtitle.ID });
                        }
                        else
                        {
                            //No srt file was uploaded
                            _SubtitleRepo.Save();
                            return RedirectToAction("Edit", new { id = NewSubtitle.ID });
                        }
                    }
                }
                //if SubtitleProfile does not exists in DB
                else
                {
                    var NewSubtitleProfile = new SubtitleProfile()
                    {
                        Title = model.Title,
                        Year = model.Year,
                        TypeID = model.TypeID
                    };
                    //Adding movieDB id 
                    if (model.TypeID == 1)//movie
                    {
                        var TheMovie = GetMovieFromTheMovieDB(NewSubtitleProfile);
                        //MovieDB ID found
                        if (TheMovie != null)
                        {
                            NewSubtitleProfile.MovieDBID = TheMovie.Id;
                            NewSubtitleProfile.Title = TheMovie.Title;
                        }
                        
                    }
                    if (model.TypeID == 2)//TV-Show
                    {
                        var TheTvShow = GetTvShowFromTheMovieDB(NewSubtitleProfile);
                        //MovieDB ID found
                        if (TheTvShow != null)
                        {
                            NewSubtitleProfile.MovieDBID = TheTvShow.Id;
                            NewSubtitleProfile.Title = TheTvShow.Name;
                        }
                    }
                    //video url was uploaded
                    if(model.VideoUrl != null){
                        Regex YT = new Regex(@"youtu(?:\.be|be\.com)/(?:.*v(?:/|=)|(?:.*/)?)([a-zA-Z0-9-_]+)");
                        Match youTube = YT.Match(model.VideoUrl);
                        if(youTube.Success)
                        {
                            NewSubtitleProfile.YouTubeURL = youTube.Groups[1].Value;
                        }
                    }
                    //Adding SubtitleProfile to DB
                    _ProfileRepo.Add(NewSubtitleProfile);
                    //Saves SubtitleProfile to DB

                    //Create new Subtitle with new Language
                    Subtitle NewSubtitle = new Subtitle
                    {
                        Complete = false,
                        SubtitleProfileID = NewSubtitleProfile.ID,
                        SubtitleProfile = NewSubtitleProfile,
                        LanguageID = model.LanguageID
                    };
                    //Creating list of subtitles
                    NewSubtitleProfile.Subtitles = new List<Subtitle>();
                    //Adding Subtitle to AddedSubtitleProfile in DB
                    NewSubtitleProfile.Subtitles.Add(NewSubtitle);

                    //SRT file has been uploaded
                    if (model.SrtUpload != null)
                    {
                        //Upload Srt file
                        if (!UploadSrt(NewSubtitle, model.SrtUpload))
                        {
                            //if failed
                            return View("AddSubtitleError");
                        }
                        //Should go to edit page
                        return RedirectToAction("Edit", new { id = NewSubtitle.ID });
                    }
                    else
                    {
                        //No srt file was uploaded
                        _SubtitleRepo.Save();
                        return RedirectToAction("Edit", new { id = NewSubtitle.ID });
                    }
                }
            }

            // If we got this far, something failed
            return View("AddSubtitleError");
        }

        /*
         * Creates a .srt from DB
         */
        public FileStreamResult Download(int? id)
        {
            StringBuilder content = new StringBuilder();

            // Get all lines for this subtitle from the Database
            var Lines = (from l in _LineRepo.GetAll()
                         where l.SubtitleID == id.Value
                         orderby l.TimeStartTicks
                         select l).ToList();

            // Register a download to Database to sort Top Subtitles
            _DownloadRepo.Add(new Download { SubtitleProfileID = Lines.First().Subtitle.SubtitleProfileID });
            _DownloadRepo.Save();

            string FormatString = @"hh\:mm\:ss\,FFFFFF";

            // Building a string that will be written to file
            for (int i = 0; i < Lines.Count(); ++i)
            {
                content.AppendLine((i + 1).ToString());
                content.Append(Lines[i].TimeStart.ToString(FormatString));
                content.Append(" --> ");
                content.AppendLine(Lines[i].TimeEnd.ToString(FormatString));
                content.AppendLine(Lines[i].Line1);
                content.AppendLine(Lines[i].Line2);
                content.AppendLine("");
            }

            var byteArray = Encoding.Unicode.GetBytes(content.ToString());
            var stream = new MemoryStream(byteArray);

            return File(stream, "text/plain", Lines.First().Subtitle.SubtitleProfile.Title + ".srt");
        }

        /*
         * Processes .srt file and saves it to DB
         */
        [Authorize]
        public bool UploadSrt(Subtitle NewSubtitle, HttpPostedFileBase SrtFile) 
        {
            NewSubtitle.Lines = new List<Line>();
            //Turning SRT file to string
            BinaryReader b = new BinaryReader(SrtFile.InputStream);
            var binData = new byte[SrtFile.InputStream.Length];
            SrtFile.InputStream.Read(binData, 0, (int)SrtFile.InputStream.Length);
            string result = System.Text.Encoding.UTF8.GetString(binData);

            //Splitting string on newlines
            string[] lines = Regex.Split(result, "\r\n");
            DateTime DateNow = DateTime.Now;
            int counter = 0;
            var NewLine = new Line() { Date = DateNow };
            foreach (string line in lines)
            {
                if (string.IsNullOrEmpty(line))
                {
                    //line is empty
                    counter = 0;
                    //Adds Line to DB
                    if (NewLine.Line1 != null)
                    {
                        NewSubtitle.Lines.Add(NewLine);
                        NewLine = new Line() { Date = DateNow };
                    }
                }
                else
                {
                    if (counter == 0)
                    {
                        //skip cause we don't need LineNr
                        counter++;
                    }
                    else if (counter == 1)
                    {
                        //Take TimeTicks
                        try 
                        { 
                            string[] timelines = line.Split(' ');
                            string[] timeline1 = timelines[0].Split(',');
                            string[] timeline2 = timelines[2].Split(',');
                            NewLine.TimeStart = TimeSpan.Parse(timeline1[0] + '.' + timeline1[1]);
                            NewLine.TimeEnd = TimeSpan.Parse(timeline2[0] + '.' + timeline2[1]);
                            counter++;
                        }
                        catch
                        {
                            return false;
                        }
                        
                    }
                    else if (counter == 2)
                    {
                        //First Line
                        NewLine.Line1 = line;
                        counter++;
                    }
                    else if (counter == 3)
                    {
                        //Second Line
                        NewLine.Line2 = line;
                        counter++;
                    }
                }
            }
            //Adds last Line to DB
            if (NewLine.Line1 != null)
            {
                NewSubtitle.Lines.Add(NewLine);
            }
            //Saves to DB
            _SubtitleRepo.Save();
            return true;
        }
	}
}