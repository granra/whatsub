﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Whatsub.Repositories;
using Whatsub.Models;

namespace Whatsub.Controllers
{
    public class HomeController : ParentController
    {
        public HomeController() : base() { }
        public HomeController(IRepository<SubtitleProfile> ProfileRepo) : base(ProfileRepo) { }
        //All repos and constructors put in ParentController

        public ActionResult Index()
        {
            IndexViewModel Model = new IndexViewModel();
            // Get newest subtitle
            Model.NewSubtitles = (from P in _ProfileRepo.GetAll()
                                from S in P.Subtitles
                                orderby S.ID descending
                                select new ProfileView {
                                    ID = S.ID,
                                    Title = P.Title,
                                    Year = P.Year
                                }).Take(5).ToList();
            // Get top subtitles
            var TopSubtitles = (from Profile in _ProfileRepo.GetAll()
                                  where Profile.Subtitles.Count() != 0
                                  orderby Profile.Downloads.Count() descending
                                  select Profile).Take(5).ToList();

            Model.TopSubtitles = new List<ProfileView>();
            // add top subtitles to list
            foreach (var Top in TopSubtitles)
            {
                Model.TopSubtitles.Add(new ProfileView 
                { 
                    ID = Top.Subtitles.First().ID, 
                    Title = Top.Title, 
                    Year = Top.Year 
                });
            }

            return View(Model);
        }

        public ActionResult Help()
        {
            ViewBag.Message = "Help page.";
            return View();
        }
        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}