﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Whatsub.Repositories;
using Whatsub.Models;
using TMDbLib.Client;
using TMDbLib.Objects.Movies;
using TMDbLib.Objects.General;
using System.Text;
using System.IO;
using System.Text.RegularExpressions;
using Microsoft.AspNet.Identity;

namespace Whatsub.Controllers
{
    public class RequestController : ParentController
    {
        public RequestController() : base() { }
        public RequestController(IRepository<SubtitleProfile> ProfileRepo) : base(ProfileRepo) { }
        public ActionResult Index(int? Page)
        {
            if (!Page.HasValue)
            {
                Page = 1;
            }
            // Get all requests and orders the by upvotes
            var Requests = (from p in _ProfileRepo.GetAll()
                            where p.Requests.Count() != 0
                            from r in p.Requests
                            orderby r.Upvotes.Count() descending
                            select new
                            {
                                ID = r.ID,
                                Title = p.Title,
                                Year = p.Year,
                                Upvotes = r.Upvotes.Count(),
                                MovieDBID = p.MovieDBID,
                                Language = r.Language,
                                TypeID = p.TypeID
                            }).Skip((Page.Value - 1) * 10).Take(10).ToList();

            PageModel<RequestViewModel> Model = new PageModel<RequestViewModel>();
            Model.Profiles = new List<RequestViewModel>();

            Model.Total = (from P in _ProfileRepo.GetAll()
                           where P.Requests.Count != 0
                           select P).Count();
            Model.Pages = (int)Math.Ceiling((double)Model.Total / 10);
            Model.CurrentPage = Page.Value;
            //Adds requests to list
            foreach (var R in Requests)
            {
                // checks if a request has a MovieDBID
                if (R.MovieDBID != null)
                {
                    // if id is 1 add information from moviDB as movie
                    if(R.TypeID == 1)
                    {
                        var Movie = _TMDb.GetMovie(R.MovieDBID.Value);
                        Model.Profiles.Add(new RequestViewModel 
                        { 
                            ID = R.ID, 
                            Title = Movie.Title, 
                            Year = R.Year, 
                            Upvotes = R.Upvotes, 
                            PosterPath = Movie.PosterPath,
                            LanguageName = R.Language.LanguageName,
                            LanguageID = R.Language.ID,
                            LanguageClass = R.Language.LanguageClass,
                            TypeID = R.TypeID
                        });
                    }
                    // id id is 2 add information from movieDB as tvshow
                    else
                    {
                        var TvShow = _TMDb.GetTvShow(R.MovieDBID.Value);
                        Model.Profiles.Add(new RequestViewModel {
                            ID = R.ID,
                            Title = TvShow.Name,
                            Year = R.Year, 
                            Upvotes = R.Upvotes, 
                            PosterPath = TvShow.PosterPath, 
                            LanguageName = R.Language.LanguageName,
                            LanguageID =R.Language.ID,
                            LanguageClass = R.Language.LanguageClass,
                            TypeID = R.TypeID
                        });
                    }
                }
                // Is there is no movie DB info then it will just have information from user
                else
                {
                    Model.Profiles.Add(new RequestViewModel {
                        ID = R.ID,
                        Title = R.Title,
                        Year = R.Year, 
                        Upvotes = R.Upvotes,
                        LanguageName = R.Language.LanguageName, 
                        LanguageClass = R.Language.LanguageClass 
                    });
                }
            }

            return View(Model);
        }
        /*
         * Simmilar to search query in sub contrtoller
         * Join requests with profile id and show 10 requests in alphabetical order
         */
        public ActionResult Search(string SearchQuery)
        {
            if (string.IsNullOrEmpty(SearchQuery))
            {
                ModelState.AddModelError("", "");
            }
            // Get all requests that match the title in SearchQuery
            var Requests = (from P in _ProfileRepo.GetAll()
                            where P.Requests.Count() != 0 
                            && P.Title.ToLower().Contains(SearchQuery.ToLower())
                            from R in P.Requests
                            orderby P.Title
                            select new
                            {
                                ID = R.ID,
                                Title = P.Title,
                                Year = P.Year,
                                Upvotes = R.Upvotes.Count(),
                                MovieDBID = P.MovieDBID,
                                Language = R.Language,
                                TypeID = P.TypeID
                            }).Take(10).ToList();

            PageModel<RequestViewModel> Model = new PageModel<RequestViewModel>();
            Model.Profiles = new List<RequestViewModel>();

            Model.CurrentPage = 0;
            Model.Pages = 0;
            Model.Total = 0;
            //Adds requests to list
            foreach (var R in Requests)
            {
                if (R.MovieDBID != null)
                {
                    //Gets movie DB info if TypeID is 1(movie)
                    if (R.TypeID == 1)
                    {
                        var Movie = _TMDb.GetMovie(R.MovieDBID.Value);
                        Model.Profiles.Add(new RequestViewModel {
                            ID = R.ID, 
                            Title = R.Title, 
                            Year = R.Year, 
                            Upvotes = R.Upvotes, 
                            PosterPath = Movie.PosterPath, 
                            LanguageName = R.Language.LanguageName,
                            LanguageClass = R.Language.LanguageClass
                        });
                    }
                    //Gets movie DB info if TypeID is 2(TvShow)
                    else
                    {
                        var TvShow = _TMDb.GetTvShow(R.MovieDBID.Value);
                        Model.Profiles.Add(new RequestViewModel { 
                            ID = R.ID, 
                            Title = R.Title, 
                            Year = R.Year, 
                            Upvotes = R.Upvotes, 
                            PosterPath = TvShow.PosterPath, 
                            LanguageName = R.Language.LanguageName,
                            LanguageClass = R.Language.LanguageClass
                        });
                    }
                }
                // Is there is no movie DB info then it will just have information from user
                else
                {
                    Model.Profiles.Add(new RequestViewModel { 
                        ID = R.ID, 
                        Title = R.Title, 
                        Year = R.Year, 
                        Upvotes = R.Upvotes,
                        LanguageName = R.Language.LanguageName,
                        LanguageClass = R.Language.LanguageClass
                    });
                }
            }
            
            return View("Index", Model);

        }
        public ActionResult Add()
        {
            // adds types of video(movie and tvshow)
            List<SelectListItem> types = new List<SelectListItem>();
            types.Add(new SelectListItem { Text = "Movie", Value = "1" });
            types.Add(new SelectListItem { Text = "TV-Series", Value = "2" });
            ViewBag.TypeID = types;
            
            // lists the available languages from data base
            List<Language> LanguageFromDB = (from l in _LanguageRepo.GetAll()
                                             select l).ToList();
            List<SelectListItem> Language = new List<SelectListItem>();

            foreach (Language l in LanguageFromDB)
            {
                SelectListItem NewSelectListItem = new SelectListItem
                {
                    Text = l.LanguageName,
                    Value = l.ID.ToString()
                };
                Language.Add(NewSelectListItem);
            }
            ViewBag.LanguageID = Language;

            return View(new RequestViewModel());
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Add(RequestViewModel Model)
        {
            if(ModelState.IsValid)
            {
                //Get subtitle profile with model.title
                var OldSubtitleProfile = GetSubtitleProfileFromTitle(Model.Title);

                // if subtitle profile exists in DB
                if(OldSubtitleProfile != null)
                {
                    //Get Subtitle with Profile ID and Language ID
                    var OldSubtitle = GetSubtitleFromSubtitleProfileIDAndLanguageID(OldSubtitleProfile.ID, Model.LanguageID);
                    // if the specific language profile exist
                    if(OldSubtitle != null)
                    {
                        //go to profile
                        return RedirectToAction("View", "Subtitle", new { id = OldSubtitle.ID });
                    }
                    //if the specific language profile does not exist
                    else
                    {
                        //Check if request for specific language profile is in DB
                        var RequestsFound = GetRequestFromSubtitleProfileIDAndLanguageID(OldSubtitleProfile.ID, Model.LanguageID);
                        if (RequestsFound != null)
                        {
                            //Request found go to Index
                            return RedirectToAction("Index");
                        }
                        else
                        {
                            //Make new Request
                            var NewRequest = new SubtitleRequest
                            {
                                LanguageID = Model.LanguageID,
                                SubtitleProfileID = OldSubtitleProfile.ID
                            };
                            //if there are no previous requests for this profile, create new list
                            if (OldSubtitleProfile.Requests == null)
                            {
                                OldSubtitleProfile.Requests = new List<SubtitleRequest>();
                            }
                            // add new request and save to DB
                            OldSubtitleProfile.Requests.Add(NewRequest);
                            _RequestRepo.Save();
                            //Should go to newest requests TODO
                            return RedirectToAction("Index");
                        }
                    }
                }
                // if subtitleprofile does not exist
                else
                {
                    //Create new Profile
                    var NewSubtitleProfile = new SubtitleProfile 
                    { 
                        Title = Model.Title,
                        Year = Model.Year,
                        TypeID = Model.TypeID
                    };
                    //If TypeID is 1, add movieDBID and title from movieDB
                    //movie
                    if(Model.TypeID == 1)
                    {
                        var TheMovie = GetMovieFromTheMovieDB(NewSubtitleProfile);
                        if(TheMovie != null)
                        {
                            NewSubtitleProfile.MovieDBID = TheMovie.Id;
                            NewSubtitleProfile.Title = TheMovie.Title;
                        }
                    }
                    //Tv show
                    if(Model.TypeID == 2)
                    {
                        var TheTvShow = GetTvShowFromTheMovieDB(NewSubtitleProfile);
                        if(TheTvShow != null)
                        {
                            NewSubtitleProfile.MovieDBID = TheTvShow.Id;
                            NewSubtitleProfile.Title = TheTvShow.Name;
                        }
                    }
                    //add the profile to DB
                    _ProfileRepo.Add(NewSubtitleProfile);

                    // create new request
                    var NewRequest = new SubtitleRequest
                    {
                        LanguageID = Model.LanguageID,
                        SubtitleProfileID = NewSubtitleProfile.ID
                    };
                    // create new list and add the request and save it
                    NewSubtitleProfile.Requests = new List<SubtitleRequest>();
                    NewSubtitleProfile.Requests.Add(NewRequest);
                    _RequestRepo.Save();
                    //Should go to newest requests TODO
                    return RedirectToAction("Index");
                }
            }
            // If we got this far, something failed, redisplay form
            return View("AddRequestError");
        }
	}
}