﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TMDbLib.Client;
using TMDbLib.Objects.General;
using Whatsub.Models;
using Whatsub.Repositories;
using System.Text.RegularExpressions;

namespace Whatsub.Controllers
{
    public class ParentController : Controller
    {
        private readonly ApplicationDbContext _context;
        protected readonly IRepository<SubtitleProfile> _ProfileRepo;
        protected readonly IRepository<Download> _DownloadRepo;
        protected readonly IRepository<Comment> _CommentRepo;
        protected readonly IRepository<SubtitleRequest> _RequestRepo;
        protected readonly IRepository<Line> _LineRepo;
        protected readonly IRepository<Subtitle> _SubtitleRepo;
        protected readonly IRepository<Upvote> _UpvoteRepo;
        protected readonly IRepository<Language> _LanguageRepo;
        protected readonly UserRepository _UserRepo;
        protected readonly TMDbClient _TMDb;
        public ParentController()
        {
            // Create the context that talks to DB
            _context = new ApplicationDbContext();
            // Create an instance of all Repositories, all using the same context
            _ProfileRepo = new ProfileRepository(_context); 
            _DownloadRepo = new DownloadRepository(_context);
            _CommentRepo = new CommentRepository(_context);
            _RequestRepo = new RequestRepository(_context);
            _LineRepo = new LineRepository(_context);
            _SubtitleRepo = new SubtitleRepository(_context);
            _UpvoteRepo = new UpvoteRepository(_context);
            _UserRepo = new UserRepository(_context);
            _LanguageRepo = new LanguageRepository(_context);
            // Create a MovieDB api client
            _TMDb = new TMDbClient("2bb138a63e105333f4f9260c69b6da4c");
        }

        public ParentController(IRepository<SubtitleProfile> ProfileRepo,
                                IRepository<Subtitle> SubtitleRepo = null,
                                IRepository<Download> DownloadRepo = null,
                                IRepository<Comment> CommentRepo = null,
                                IRepository<SubtitleRequest> RequestRepo = null,
                                IRepository<Line> LineRepo = null,
                                IRepository<Upvote> UpvoteRepo = null)
        {
            _ProfileRepo = ProfileRepo;
            _DownloadRepo = DownloadRepo;
            _CommentRepo = CommentRepo;
            _RequestRepo = RequestRepo;
            _LineRepo = LineRepo;
            _SubtitleRepo = SubtitleRepo;
            _UpvoteRepo = UpvoteRepo;

            _TMDb = new TMDbClient("2bb138a63e105333f4f9260c69b6da4c");
        }
        //Get Movie from TheMovieDB
        public TMDbLib.Objects.Search.SearchMovie GetMovieFromTheMovieDB(SubtitleProfile NewSubtitleProfile) 
        {
            SearchContainer<TMDbLib.Objects.Search.SearchMovie> Movies = _TMDb.SearchMovie(NewSubtitleProfile.Title);
            var TheMovie = (from m in Movies.Results
                            where m.Title.ToLower() == NewSubtitleProfile.Title.ToLower() && m.ReleaseDate.Value.Year == NewSubtitleProfile.Year
                            select m).FirstOrDefault();
           return TheMovie;
        }
        //Get TvShow from TheMovieDB
        public TMDbLib.Objects.TvShows.TvShowBase GetTvShowFromTheMovieDB(SubtitleProfile NewSubtitleProfile) 
        {
            SearchContainer<TMDbLib.Objects.TvShows.TvShowBase> TvShows = _TMDb.SearchTvShow(NewSubtitleProfile.Title);
            var TheTvShow = (from m in TvShows.Results
                             where m.Name.ToLower() == NewSubtitleProfile.Title.ToLower()
                             select m).FirstOrDefault();
            return TheTvShow;
        }

        //Get SubtitleProfile with string Title
        public SubtitleProfile GetSubtitleProfileFromTitle(string Title) 
        {
            var SubtitleProfileFromTitle = (    from s in _ProfileRepo.GetAll()
                                                where s.Title == Title
                                                select s).SingleOrDefault();
            return SubtitleProfileFromTitle;
        }
        //Get Subtitle From SubtitleProfileID and LanguageID
        public Subtitle GetSubtitleFromSubtitleProfileIDAndLanguageID(int SubtitleProfileID, int LanguageID)
        {
            var FoundSubtitle = (   from s in _SubtitleRepo.GetAll()
                                    where s.LanguageID == LanguageID && 
                                    s.SubtitleProfileID == SubtitleProfileID
                                    select s).SingleOrDefault();
            return FoundSubtitle;
        }
        //Get Request From SubtitleProfileID and LanguageID
        public SubtitleRequest GetRequestFromSubtitleProfileIDAndLanguageID(int SubtitleProfileID, int LanguageID) 
        {
            SubtitleRequest FoundRequest = (from r in _RequestRepo.GetAll()
                                            where r.LanguageID == LanguageID
                                            && r.SubtitleProfileID == SubtitleProfileID
                                            select r).SingleOrDefault();
            return FoundRequest;
        }

        //Get Id from youtube url
        public string GetYoutubeID(string YoutubeUrl) 
        {
            string UrlID = Regex.Match(YoutubeUrl, @"(?:youtube\.com\/(?:[^\/]+\/.+\/|
                        (?:v|e(?:mbed)?)\/|.*[?&amp;]v=)|youtu\.be\/)([^""&amp;?\/ ]{11})").Groups[1].Value;
            return UrlID;
        }

	}
}