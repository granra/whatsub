﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Whatsub.Models;
using Whatsub.Repositories;

namespace Whatsub.FakeRepo
{
    public class FakeRequestRepository : IRepository<SubtitleRequest>
    {
        private List<SubtitleRequest> _Requests;

        public FakeRequestRepository()
        {
            _Requests = new List<SubtitleRequest>();

            _Requests.Add(new SubtitleRequest { ID = 1, ProfileID = 2, LanguageID = 1 });
            _Requests.Add(new SubtitleRequest { ID = 2, ProfileID = 4, LanguageID = 1 });
            _Requests.Add(new SubtitleRequest { ID = 3, ProfileID = 3, LanguageID = 1 });
            _Requests.Add(new SubtitleRequest { ID = 4, ProfileID = 1, LanguageID = 1 });
            _Requests.Add(new SubtitleRequest { ID = 5, ProfileID = 7, LanguageID = 1 });
            _Requests.Add(new SubtitleRequest { ID = 6, ProfileID = 5, LanguageID = 1 });
            _Requests.Add(new SubtitleRequest { ID = 7, ProfileID = 10, LanguageID = 1 });
        }

        public IQueryable<SubtitleRequest> GetAll()
        {
            return _Requests.AsQueryable();
        }

        public void Add(SubtitleRequest NewSubtitleRequest)
        {
            _Requests.Add(NewSubtitleRequest);
        }

        public void Delete(SubtitleRequest SubtitleRequest)
        {
            // TODO
        }

        public void Edit(SubtitleRequest SubtitleRequest)
        {
            // TODO
        }

        public void Save()
        {
            // TODO
        }
    }
}