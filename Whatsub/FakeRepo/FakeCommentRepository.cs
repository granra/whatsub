﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Whatsub.Repositories;
using Whatsub.Entity;

namespace Whatsub.FakeRepo
{
    public class FakeCommentRepository : IRepository<Comment>
    {
        private List<Comment> _Comments;

        public FakeCommentRepository()
        {
            _Comments = new List<Comment>();
            _Comments.Add(new Comment { ID = 1, SubtitleID = 2, CommentText = "Frozen comment 1", UserID = 1});
            _Comments.Add(new Comment { ID = 2, SubtitleID = 2, CommentText = "Frozen comment 2", UserID = 1 });
            _Comments.Add(new Comment { ID = 3, SubtitleID = 2, CommentText = "Frozen comment 3", UserID = 1 });
            _Comments.Add(new Comment { ID = 4, SubtitleID = 2, CommentText = "Frozen comment 4", UserID = 1 });
        }
        public IQueryable<Comment> GetAll()
        {
            return _Comments.AsQueryable();
        }
        public void Add(Comment NewComment)
        {
            _Comments.Add(NewComment);
        }
        public void Delete(Comment Comment)
        {
            //TODO
        }
        public void Edit(Comment Comment)
        {
            //TODO
        }
        public void Save()
        {
            //TODO
        }


    }
}