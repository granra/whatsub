﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Whatsub.Models;
using Whatsub.Repositories;

namespace Whatsub.FakeRepo
{
    public class FakeUpvoteRepository : IRepository<Upvote>
    {
        private List<Upvote> _Upvotes;

        public FakeUpvoteRepository()
        {
            _Upvotes = new List<Upvote>();

            _Upvotes.Add(new Upvote { ID = 1, SubtitleRequestID = 2 });
            _Upvotes.Add(new Upvote { ID = 2, SubtitleRequestID = 2 });
            _Upvotes.Add(new Upvote { ID = 3, SubtitleRequestID = 3 });
            _Upvotes.Add(new Upvote { ID = 4, SubtitleRequestID = 2 });
            _Upvotes.Add(new Upvote { ID = 5, SubtitleRequestID = 2 });
        }

        public IQueryable<Upvote> GetAll()
        {
            return _Upvotes.AsQueryable();
        }

        public void Add(Upvote NewUpvote)
        {
            _Upvotes.Add(NewUpvote);
        }

        public void Delete(Upvote Upvote)
        {
            // TODO
        }

        public void Edit(Upvote Upvote)
        {
            // TODO
        }

        public void Save()
        {
            // TODO
        }
    }
}