﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Whatsub.Repositories;
using Whatsub.Models;

namespace Whatsub.FakeRepo
{
    public class FakeProfileRepository : IRepository<SubtitleProfile>
    {
        private List<SubtitleProfile> _Profiles;
        public FakeProfileRepository()
        {
            _Profiles = new List<SubtitleProfile>();

            _Profiles.Add(new SubtitleProfile { ID = 1, Title = "Timecop", Year = 1994, TypeID = 1, MovieDBID = 8831 });
            _Profiles.Add(new SubtitleProfile { ID = 2, Title = "Frozen", Year = 2013, TypeID = 1, MovieDBID = 109445 });
            _Profiles.Add(new SubtitleProfile { ID = 3, Title = "Timecop 2", Year = 2003, TypeID = 1, MovieDBID = 18051 });
            _Profiles.Add(new SubtitleProfile { ID = 4, Title = "Toy Story 3", Year = 2010, TypeID = 1, MovieDBID = 10193 });
            _Profiles.Add(new SubtitleProfile { ID = 5, Title = "The Amazing Spider-Man 2", Year = 2014, TypeID = 1, MovieDBID = 102382 });
            _Profiles.Add(new SubtitleProfile { ID = 6, Title = "Payback", Year = 1999, TypeID = 1, MovieDBID = 2112 });
            _Profiles.Add(new SubtitleProfile { ID = 7, Title = "Paycheck", Year = 2003, TypeID = 1, MovieDBID = 9620 });
            _Profiles.Add(new SubtitleProfile { ID = 8, Title = "Ghost Ship", Year = 2002, TypeID = 1, MovieDBID = 9645 });
            _Profiles.Add(new SubtitleProfile { ID = 9, Title = "The Fifth Element", Year = 1997, TypeID = 1, MovieDBID = 18 });
            _Profiles.Add(new SubtitleProfile { ID = 10, Title = "Neighbors", Year = 2014, TypeID = 1, MovieDBID = 195589 });
        }

        public IQueryable<SubtitleProfile> GetAll()
        {
            return _Profiles.AsQueryable();
        }

        public void Add(SubtitleProfile NewSubtitleProfile)
        {
            _Profiles.Add(NewSubtitleProfile);
        }

        public void Delete(SubtitleProfile SubtitleProfile)
        {
            // TODO
        }

        public void Edit(SubtitleProfile SubtitleProfile)
        {
            // TODO
        }

        public void Save()
        {
            // TODO
        }
    }
}