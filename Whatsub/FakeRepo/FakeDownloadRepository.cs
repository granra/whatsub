﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Whatsub.Models;
using Whatsub.Repositories;

namespace Whatsub.FakeRepo
{
    public class FakeDownloadRepository : IRepository<Download>
    {
        private List<Download> _Downloads;

        public FakeDownloadRepository()
        {
            _Downloads = new List<Download>();

            _Downloads.Add(new Download { ID = 1, ProfileID = 2 });
            _Downloads.Add(new Download { ID = 2, ProfileID = 2 });
            _Downloads.Add(new Download { ID = 3, ProfileID = 1 });
            _Downloads.Add(new Download { ID = 4, ProfileID = 5 });
            _Downloads.Add(new Download { ID = 5, ProfileID = 10 });
            _Downloads.Add(new Download { ID = 6, ProfileID = 5 });
            _Downloads.Add(new Download { ID = 7, ProfileID = 8 });
            _Downloads.Add(new Download { ID = 8, ProfileID = 2 });
            _Downloads.Add(new Download { ID = 9, ProfileID = 3 });
            _Downloads.Add(new Download { ID = 10, ProfileID = 6 });
            _Downloads.Add(new Download { ID = 11, ProfileID = 4 });
            _Downloads.Add(new Download { ID = 12, ProfileID = 3 });
            _Downloads.Add(new Download { ID = 13, ProfileID = 8 });
            _Downloads.Add(new Download { ID = 14, ProfileID = 10 });
            _Downloads.Add(new Download { ID = 15, ProfileID = 4 });
            _Downloads.Add(new Download { ID = 16, ProfileID = 7 });
        }

        public IQueryable<Download> GetAll()
        {
            return _Downloads.AsQueryable();
        }

        public void Add(Download NewDownload)
        {
            _Downloads.Add(NewDownload);
        }

        public void Delete(Download Download)
        {
            // TODO
        }

        public void Edit(Download Download)
        {
            // TODO
        }

        public void Save()
        {
            // TODO
        }
    }
}