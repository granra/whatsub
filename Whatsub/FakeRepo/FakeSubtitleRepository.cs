﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Whatsub.Repositories;
using Whatsub.Models;

namespace Whatsub.FakeRepo
{
    public class FakeSubtitleRepository : IRepository<Subtitle>
    {
        private List<Subtitle> _Subtitles;

        public FakeSubtitleRepository()
        {
            _Subtitles = new List<Subtitle>();

            _Subtitles.Add(new Subtitle { ID = 1, ProfileID = 6, LanguageID = 1, Complete = false });
            _Subtitles.Add(new Subtitle { ID = 2, ProfileID = 9, LanguageID = 1, Complete = true });
            _Subtitles.Add(new Subtitle { ID = 3, ProfileID = 8, LanguageID = 1, Complete = false });
        }

        public IQueryable<Subtitle> GetAll()
        {
            return _Subtitles.AsQueryable();
        }

        public void Add(Subtitle NewSubtitle)
        {
            _Subtitles.Add(NewSubtitle);
        }

        public void Delete(Subtitle Subtitle)
        {
            // TODO
        }

        public void Edit(Subtitle Subtitle)
        {
            // TODO
        }

        public void Save()
        {
            // TODO
        }
    }
}