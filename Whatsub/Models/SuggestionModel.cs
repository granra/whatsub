﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Whatsub.Models
{
    public class SuggestionModel<T> where T : class
    {
        public string query { get; set; }
        public string[] suggestions { get; set; }
        public T[] data { get; set; }
    }
}