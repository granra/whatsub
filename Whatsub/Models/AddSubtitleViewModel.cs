﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Whatsub.Models
{
    public class AddSubtitleViewModel
    {
        [Required]
        public string Title { get; set; }
        [Required]
        public int TypeID { get; set; }
        [Required]
        public int Year { get; set; }
        [Required]
        public int LanguageID { get; set; }
        public HttpPostedFileBase SrtUpload { get; set; }
        [DataType(DataType.Url)]
        public string VideoUrl { get; set; }
    }
}