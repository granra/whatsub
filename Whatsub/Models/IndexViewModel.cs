﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Whatsub.Models;

namespace Whatsub.Models
{
    public class IndexViewModel
    {
        public List<ProfileView> TopSubtitles { get; set; }
        public List<ProfileView> NewSubtitles { get; set; }
    }

    public class ProfileView
    {
        public int ID { get; set; }
        public string Title { get; set; }
        public int Year { get; set; }
        public string LanguageClass { get; set; }
    }
}