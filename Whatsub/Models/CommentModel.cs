﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Whatsub.Models
{
    public class CommentModel
    {
        public string Username { get; set; }
        public string Comment { get; set; }
    }
}