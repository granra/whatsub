﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Whatsub.Models
{
    public class SubtitleProfile
    {
        public int ID { get; set; }
        public string Title { get; set; }
        public int Year { get; set; }
        public int? MovieDBID { get; set; }
        public int TypeID { get; set; }
        public string YouTubeURL { get; set; }

        public virtual List<SubtitleRequest> Requests { get; set; }
        public virtual List<Subtitle> Subtitles { get; set; }
        public virtual List<Download> Downloads { get; set; }
    }

    public class SubtitleRequest
    {
        public int ID { get; set; }

        // Foreign Keys
        public int SubtitleProfileID { get; set; }
        public virtual SubtitleProfile SubtitleProfile { get; set; }
        public int LanguageID { get; set; }
        public virtual Language Language { get; set; }

        public virtual List<Upvote> Upvotes { get; set; }
    }

    public class Subtitle
    {
        public int ID { get; set; }
        public bool Complete { get; set; }

        // Foreign Keys
        public int SubtitleProfileID { get; set; }
        public virtual SubtitleProfile SubtitleProfile { get; set; }
        public int LanguageID { get; set; }
        public virtual Language Language { get; set; }

        public virtual List<Line> Lines { get; set; }
        public virtual List<Comment> Comments { get; set; }
    }

    public class Line
    {
        public int ID { get; set; }
        public Int64 TimeStartTicks { get; set; }
        public Int64 TimeEndTicks { get; set; }
        public string Line1 { get; set; }
        public string Line2 { get; set; }
        public DateTime Date { get; set; }

        // Foreign Keys
        public int SubtitleID { get; set; }
        public virtual Subtitle Subtitle { get; set; }
        public string ApplicationUserID { get; set; }
        public virtual ApplicationUser ApplitcationUser { get; set; }

        [NotMapped]
        public TimeSpan TimeStart
        {
            get { return TimeSpan.FromTicks(TimeStartTicks); }
            set { TimeStartTicks = value.Ticks; }
        }
        [NotMapped]
        public TimeSpan TimeEnd
        {
            get { return TimeSpan.FromTicks(TimeEndTicks); }
            set { TimeEndTicks = value.Ticks; }
        }
    }

    public class Upvote
    {
        public int ID { get; set; }

        // Foreign Keys
        public int SubtitleRequestID { get; set; }
        public virtual SubtitleRequest SubtitleRequest { get; set; }
    }

    public class Comment
    {
        public int ID { get; set; }
        public string CommentText { get; set; }
        public DateTime Date { get; set; }

        // Foreign Keys
        public int SubtitleID { get; set; }
        public virtual Subtitle Subtitle { get; set; }
        public string ApplicationUserID { get; set; }
        public virtual ApplicationUser ApplicationUser { get; set; }
    }

    public class Download
    {
        public int ID { get; set; }

        // Foreign Keys
        public int SubtitleProfileID { get; set; }
        public virtual SubtitleProfile SubtitleProfile { get; set; }
    }

    public class Language
    {
        public int ID { get; set; }
        public string LanguageName { get; set; }
        public string LanguageClass { get; set; }
    }
}