﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Whatsub.Models
{
    public class PageModel<T> where T : class
    {
        public List<T> Profiles { get; set; }
        public int Pages { get; set; }
        public int CurrentPage { get; set; }
        public int Total { get; set; }
    }
}