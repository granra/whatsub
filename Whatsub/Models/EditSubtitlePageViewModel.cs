﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Whatsub.Models
{
    public class EditSubtitlePageViewModel
    {
        /*View Model for the the Edit Subtitle Page*/
        public bool Complete { get; set; }
        public int ID { get; set; }
        public string Title { get; set; }
        public int Year { get; set; }
        public List<Line> Lines { get; set; }
        public List<Comment> Comments { get; set; }
        public int SubtitleID { get; set; }
        public int PageNumber { get; set; }
        public int Pages { get; set; }
        public int SubtitleLanguageID { get; set; }
        public string SubtitleLanguage { get; set; }
    }

    public class CompleteStatusViewModel
    {
        public int SubtitleID { get; set; }
        public bool CompleteStatus { get; set; }
    }


    public class EditSubtitleSaveModel
    {
        public int LineID { get; set; }
        public string TimeStart { get; set; }
        public string TimeEnd { get; set; }
        public string Line1 { get; set; }
        public string Line2 { get; set; }
        public int SubtitleID { get; set; }
    }

    public class AddSubtitleSaveModel
    {
        public string TimeStart { get; set; }
        public string TimeEnd { get; set; }
        public string Line1 { get; set; }
        public string Line2 { get; set; }
        public int SubtitleID { get; set; }
    }
}