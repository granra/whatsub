﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Whatsub.Models
{
    public class LanguageModel
    {
        public int SubtitleID { get; set; }
        public string LanguageName { get; set; }
    }
}