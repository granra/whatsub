﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Whatsub.Models;
using System.ComponentModel.DataAnnotations;

namespace Whatsub.Models
{
    public class RequestViewModel
    {
        [Required]
        public int ID { get; set; }
        [Required]
        public string Title { get; set; }
        [Required]
        public int TypeID { get; set; }
        [Required]
        public int Year { get; set; }
        [Required]
        public int LanguageID { get; set; }
        public string LanguageName { get; set; }
        public string LanguageClass { get; set; }
        public int Upvotes { get; set; }
        public string PosterPath { get; set; }
        [DataType(DataType.Url)]
        public string VideoUrl { get; set; }
    }
}