﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Whatsub.Models;

namespace Whatsub.Models
{
    public class SubtitleProfilePageViewModel
    {
        /*View Model for the the SubtitleProfile Page*/
        public int ID { get; set; }
        public int ProfileID { get; set; }
        public String Title { get; set; }
        public int Year { get; set; }
        public string PosterPath { get; set; }
        public string Overview { get; set; }
        public int? MovieDBID { get; set; }
        public string YoutubeUrl { get; set; }
        public bool Complete { get; set; }
        public int TypeID { get; set; }
    }
}