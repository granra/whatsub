﻿using Microsoft.AspNet.Identity.EntityFramework;
using System.Data.Entity;

namespace Whatsub.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        public string EmailAddress { get; set; }
        public int RankID { get; set; }
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection")
        {
        }

        public DbSet<SubtitleProfile> SubtitleProfiles { get; set; }
        public DbSet<SubtitleRequest> SubtitleRequests { get; set; }
        public DbSet<Subtitle> Subtitles { get; set; }
        public DbSet<Download> Downloads { get; set; }
        public DbSet<Line> Lines { get; set; }
        public DbSet<Upvote> Upvotes { get; set; }
        public DbSet<Comment> Comments { get; set; }
        public DbSet<Language> Languages { get; set; }
    }
}