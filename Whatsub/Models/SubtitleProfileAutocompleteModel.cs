﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Whatsub.Models
{
    public class SubtitleProfileAutocompleteModel
    {
        public int MovieDBID { get; set; }
        public string Title { get; set; }
        public int Year { get; set; }
    }
}