﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Whatsub.Models
{
    public class SubtitleSearchViewModel
    {
        public int ID { get; set; }
        public string Title { get; set; }
        public int Year { get; set; }
        public string PosterPath { get; set; }
    }
}