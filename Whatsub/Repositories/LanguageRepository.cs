﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Whatsub.Models;

namespace Whatsub.Repositories
{
    public class LanguageRepository : IRepository<Language>
    {

        private ApplicationDbContext _context;

        public LanguageRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public IQueryable<Language> GetAll()
        {
            return _context.Languages;
        }

        public void Add(Language NewLanguage)
        {
            _context.Languages.Add(NewLanguage);
        }

        public void Delete(Language Language)
        {
            _context.Languages.Remove(Language);
        }

        public void Save()
        {
            _context.SaveChanges();
        }
    }
}