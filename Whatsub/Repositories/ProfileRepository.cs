﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Whatsub.Models;

namespace Whatsub.Repositories
{
    public class ProfileRepository : IRepository<SubtitleProfile>
    {
        private ApplicationDbContext _context;

        public ProfileRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public IQueryable<SubtitleProfile> GetAll()
        {
            return _context.SubtitleProfiles;
        }

        public void Add(SubtitleProfile NewSubtitleProfile)
        {
            _context.SubtitleProfiles.Add(NewSubtitleProfile);
        }

        public void Delete(SubtitleProfile SubtitleProfile)
        {
            _context.SubtitleProfiles.Remove(SubtitleProfile);
        }

        public void Save()
        {
            _context.SaveChanges();
        }
    }
}