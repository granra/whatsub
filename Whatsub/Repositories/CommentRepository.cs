﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Whatsub.Models;

namespace Whatsub.Repositories
{
    public class CommentRepository : IRepository<Comment>
    {

        private ApplicationDbContext _context;

        public CommentRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public IQueryable<Comment> GetAll()
        {
            return _context.Comments;
        }

        public void Add(Comment NewComment)
        {
            _context.Comments.Add(NewComment);
        }

        public void Delete(Comment Comment)
        {
            _context.Comments.Remove(Comment);
        }

        public void Save()
        {
            _context.SaveChanges();
        }
    }
}