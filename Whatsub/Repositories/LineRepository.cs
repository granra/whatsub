﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Whatsub.Models;

namespace Whatsub.Repositories
{
    public class LineRepository : IRepository<Line>
    {
        private ApplicationDbContext _context;

        public LineRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public IQueryable<Line> GetAll()
        {
            return _context.Lines;
        }

        public void Add(Line NewLine)
        {
            _context.Lines.Add(NewLine);
        }

        public void Delete(Line Line)
        {
            _context.Lines.Remove(Line);
        }

        public void Save()
        {
            _context.SaveChanges();
        }
    }
}