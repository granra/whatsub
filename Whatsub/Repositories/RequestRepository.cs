﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Whatsub.Models;

namespace Whatsub.Repositories
{
    public class RequestRepository : IRepository<SubtitleRequest>
    {
        private ApplicationDbContext _context;

        public RequestRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public IQueryable<SubtitleRequest> GetAll()
        {
            return _context.SubtitleRequests;
        }

        public void Add(SubtitleRequest NewSubtitleRequest)
        {
            _context.SubtitleRequests.Add(NewSubtitleRequest);
        }

        public void Delete(SubtitleRequest SubtitleRequest)
        {
            _context.SubtitleRequests.Remove(SubtitleRequest);
        }

        public void Save()
        {
            _context.SaveChanges();
        }
    }
}