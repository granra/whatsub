﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Whatsub.Models;

namespace Whatsub.Repositories
{
    public class DownloadRepository : IRepository<Download>
    {
        private ApplicationDbContext _context;

        public DownloadRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public IQueryable<Download> GetAll()
        {
            return _context.Downloads;
        }

        public void Add(Download NewDownload)
        {
            _context.Downloads.Add(NewDownload);
        }

        public void Delete(Download Download)
        {
            _context.Downloads.Remove(Download);
        }

        public void Save()
        {
            _context.SaveChanges();
        }
    }
}