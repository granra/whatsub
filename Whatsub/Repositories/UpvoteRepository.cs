﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Whatsub.Models;

namespace Whatsub.Repositories
{
    public class UpvoteRepository : IRepository<Upvote>
    {
        private ApplicationDbContext _context;

        public UpvoteRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public IQueryable<Upvote> GetAll()
        {
            return _context.Upvotes;
        }

        public void Add(Upvote NewUpvote)
        {
            _context.Upvotes.Add(NewUpvote);
        }

        public void Delete(Upvote Upvote)
        {
            _context.Upvotes.Remove(Upvote);
        }

        public void Save()
        {
            _context.SaveChanges();
        }
    }
}