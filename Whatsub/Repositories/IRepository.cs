﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Whatsub.Repositories
{
    public interface IRepository<T> where T : class
    {
        IQueryable<T> GetAll();
        void Add(T entity);
        void Delete(T entity);
        void Save();
    }
}