﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Whatsub.Models;

namespace Whatsub.Repositories
{
    public class SubtitleRepository : IRepository<Subtitle>
    {
        private ApplicationDbContext _context;

        public SubtitleRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public IQueryable<Subtitle> GetAll()
        {
            return _context.Subtitles;
        }

        public void Add(Subtitle NewSubtitle)
        {
            _context.Subtitles.Add(NewSubtitle);
        }

        public void Delete(Subtitle Subtitle)
        {
            _context.Subtitles.Remove(Subtitle);
        }

        public void Save()
        {
            _context.SaveChanges();
        }
    }
}