namespace Whatsub.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemoveLineNr : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Lines", "LineNr");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Lines", "LineNr", c => c.Int(nullable: false));
        }
    }
}
