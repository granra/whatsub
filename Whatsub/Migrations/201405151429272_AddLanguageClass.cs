namespace Whatsub.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddLanguageClass : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Languages", "LanguageClass", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Languages", "LanguageClass");
        }
    }
}
