namespace Whatsub.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UserIds : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Comments", "UserID", c => c.String());
            AddColumn("dbo.Comments", "ApplicationUser_Id", c => c.String(maxLength: 128));
            AddColumn("dbo.Lines", "UserID", c => c.String());
            AddColumn("dbo.Lines", "ApplitcationUser_Id", c => c.String(maxLength: 128));
            CreateIndex("dbo.Comments", "ApplicationUser_Id");
            CreateIndex("dbo.Lines", "ApplitcationUser_Id");
            AddForeignKey("dbo.Comments", "ApplicationUser_Id", "dbo.AspNetUsers", "Id");
            AddForeignKey("dbo.Lines", "ApplitcationUser_Id", "dbo.AspNetUsers", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Lines", "ApplitcationUser_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.Comments", "ApplicationUser_Id", "dbo.AspNetUsers");
            DropIndex("dbo.Lines", new[] { "ApplitcationUser_Id" });
            DropIndex("dbo.Comments", new[] { "ApplicationUser_Id" });
            DropColumn("dbo.Lines", "ApplitcationUser_Id");
            DropColumn("dbo.Lines", "UserID");
            DropColumn("dbo.Comments", "ApplicationUser_Id");
            DropColumn("dbo.Comments", "UserID");
        }
    }
}
