namespace Whatsub.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FixUserID : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.Comments", name: "ApplicationUser_Id", newName: "ApplicationUserID");
            RenameColumn(table: "dbo.Lines", name: "ApplitcationUser_Id", newName: "ApplicationUserID");
            DropColumn("dbo.Comments", "UserID");
            DropColumn("dbo.Lines", "UserID");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Lines", "UserID", c => c.String());
            AddColumn("dbo.Comments", "UserID", c => c.String());
            RenameColumn(table: "dbo.Lines", name: "ApplicationUserID", newName: "ApplitcationUser_Id");
            RenameColumn(table: "dbo.Comments", name: "ApplicationUserID", newName: "ApplicationUser_Id");
        }
    }
}
