namespace Whatsub.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RankID : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetUsers", "RankID", c => c.Int());
        }
        
        public override void Down()
        {
            DropColumn("dbo.AspNetUsers", "RankID");
        }
    }
}
