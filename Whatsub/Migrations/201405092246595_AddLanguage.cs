namespace Whatsub.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddLanguage : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Languages",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        LanguageName = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            AddColumn("dbo.Subtitles", "LanguageID", c => c.Int(nullable: false));
            AddColumn("dbo.SubtitleProfiles", "TypeID", c => c.Int(nullable: false));
            CreateIndex("dbo.Subtitles", "LanguageID");
            CreateIndex("dbo.SubtitleRequests", "LanguageID");
            AddForeignKey("dbo.Subtitles", "LanguageID", "dbo.Languages", "ID", cascadeDelete: true);
            AddForeignKey("dbo.SubtitleRequests", "LanguageID", "dbo.Languages", "ID", cascadeDelete: true);
            DropColumn("dbo.Subtitles", "Language");
            DropColumn("dbo.SubtitleProfiles", "Type");
        }
        
        public override void Down()
        {
            AddColumn("dbo.SubtitleProfiles", "Type", c => c.Int(nullable: false));
            AddColumn("dbo.Subtitles", "Language", c => c.Int(nullable: false));
            DropForeignKey("dbo.SubtitleRequests", "LanguageID", "dbo.Languages");
            DropForeignKey("dbo.Subtitles", "LanguageID", "dbo.Languages");
            DropIndex("dbo.SubtitleRequests", new[] { "LanguageID" });
            DropIndex("dbo.Subtitles", new[] { "LanguageID" });
            DropColumn("dbo.SubtitleProfiles", "TypeID");
            DropColumn("dbo.Subtitles", "LanguageID");
            DropTable("dbo.Languages");
        }
    }
}
