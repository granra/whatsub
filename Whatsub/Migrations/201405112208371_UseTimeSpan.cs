namespace Whatsub.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UseTimeSpan : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Lines", "TimeStartTicks", c => c.Long(nullable: false));
            AddColumn("dbo.Lines", "TimeEndTicks", c => c.Long(nullable: false));
            DropColumn("dbo.Lines", "TimeStart");
            DropColumn("dbo.Lines", "TimeEnd");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Lines", "TimeEnd", c => c.String());
            AddColumn("dbo.Lines", "TimeStart", c => c.String());
            DropColumn("dbo.Lines", "TimeEndTicks");
            DropColumn("dbo.Lines", "TimeStartTicks");
        }
    }
}
