namespace Whatsub.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeContext : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Comments",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        CommentText = c.String(),
                        SubtitleID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Subtitles", t => t.SubtitleID, cascadeDelete: true)
                .Index(t => t.SubtitleID);
            
            CreateTable(
                "dbo.Subtitles",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Complete = c.Boolean(nullable: false),
                        Language = c.Int(nullable: false),
                        SubtitleProfileID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.SubtitleProfiles", t => t.SubtitleProfileID, cascadeDelete: true)
                .Index(t => t.SubtitleProfileID);
            
            CreateTable(
                "dbo.Lines",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        LineNr = c.Int(nullable: false),
                        TimeStart = c.String(),
                        TimeEnd = c.String(),
                        Line1 = c.String(),
                        Line2 = c.String(),
                        Date = c.DateTime(nullable: false),
                        SubtitleID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Subtitles", t => t.SubtitleID, cascadeDelete: true)
                .Index(t => t.SubtitleID);
            
            CreateTable(
                "dbo.SubtitleProfiles",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Title = c.String(),
                        Year = c.Int(nullable: false),
                        MovieDBID = c.Int(),
                        Type = c.Int(nullable: false),
                        YouTubeURL = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Downloads",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        SubtitleProfileID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.SubtitleProfiles", t => t.SubtitleProfileID, cascadeDelete: true)
                .Index(t => t.SubtitleProfileID);
            
            CreateTable(
                "dbo.SubtitleRequests",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        LanguageID = c.Int(nullable: false),
                        SubtitleProfileID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.SubtitleProfiles", t => t.SubtitleProfileID, cascadeDelete: true)
                .Index(t => t.SubtitleProfileID);
            
            CreateTable(
                "dbo.Upvotes",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        SubtitleRequestID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.SubtitleRequests", t => t.SubtitleRequestID, cascadeDelete: true)
                .Index(t => t.SubtitleRequestID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Subtitles", "SubtitleProfileID", "dbo.SubtitleProfiles");
            DropForeignKey("dbo.Upvotes", "SubtitleRequestID", "dbo.SubtitleRequests");
            DropForeignKey("dbo.SubtitleRequests", "SubtitleProfileID", "dbo.SubtitleProfiles");
            DropForeignKey("dbo.Downloads", "SubtitleProfileID", "dbo.SubtitleProfiles");
            DropForeignKey("dbo.Lines", "SubtitleID", "dbo.Subtitles");
            DropForeignKey("dbo.Comments", "SubtitleID", "dbo.Subtitles");
            DropIndex("dbo.Subtitles", new[] { "SubtitleProfileID" });
            DropIndex("dbo.Upvotes", new[] { "SubtitleRequestID" });
            DropIndex("dbo.SubtitleRequests", new[] { "SubtitleProfileID" });
            DropIndex("dbo.Downloads", new[] { "SubtitleProfileID" });
            DropIndex("dbo.Lines", new[] { "SubtitleID" });
            DropIndex("dbo.Comments", new[] { "SubtitleID" });
            DropTable("dbo.Upvotes");
            DropTable("dbo.SubtitleRequests");
            DropTable("dbo.Downloads");
            DropTable("dbo.SubtitleProfiles");
            DropTable("dbo.Lines");
            DropTable("dbo.Subtitles");
            DropTable("dbo.Comments");
        }
    }
}
