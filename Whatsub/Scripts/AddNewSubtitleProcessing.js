﻿$(document).ready(function () {
    // Gets file extension
    function getFileExtension(filename) {
        return filename.split('.').pop();
    }
    // Show the notification and disable the button
    $("#AddNewSubtitleForm").submit(function (event) {
        var title = $("#Title").val()
        if (!(/^\s*$/.test(title)))
        {
            $("#UploadProcessingBox").show();
            $(".btn-primary").attr("disabled", "disabled");
            return true;
        }
        else
        {
            $("#ErrorInputMessage").show();
            $('.form-group').addClass("has-error");
            return false;
        }

    });
});