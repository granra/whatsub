﻿$(document).ready(function () {

    $(".upvoteArrow").click(function () {
        var clicked = $(this);
        var SubID = $(this).data("id");
        var VoteID = $(this).siblings("p").data("id");

        if (!clicked.hasClass('upvoted')) {
            data = {
                RequestID: SubID
            }
        }
        else {
            data = {
                RequestID: SubID,
                UpvoteID: VoteID
            }
        }
        $.ajax({
            type: "POST",
            url: '/Ajax/Upvote',
            data: data,
            datatype: 'JSON',
            success: function (result) {
                clicked.toggleClass('upvoted');
                clicked.siblings("p").html(result.Upcount);
                clicked.siblings("p").data("id", result.id);
            },
            error: function (xhr, err) {
                // Note: just for debugging purposes!
                alert("readyState: " + xhr.readyState +
                "\nstatus: " + xhr.status);
                alert("responseText: " + xhr.responseText);
            }
        });

        return false;
    });

    function updateCount(data) {
        $(id).find(".upvote-count").html(data);
    }

});