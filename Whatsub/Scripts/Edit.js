﻿$(document).ready(function () {
    //Changing the status of the subtitle
    $("#CompleteStatus").change(function () {
        var CompleteStatusViewModel = {
            SubtitleID: $("#SubtitleID").val(),
            CompleteStatus: $("#CompleteStatus").val()
        };
        $.ajax({
            type: "GET",
            url: "/Ajax/CompleteStatus",
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            data: CompleteStatusViewModel,
            success: function (data) {
                //show user it was successful
                $("#CompleteChanged").fadeIn(200);
                $("#CompleteChanged").fadeOut(2000);
            },
            error: function () {
                alert("Error! Please call 0118 999 881 999 119 7253");
            }
        });
    });
});

$(document).bind('ready ajaxComplete', function () {
    //When line is clicked in table update editor
    $(".SubtitleLine").click(function () {
        $("#LineID").val($(this).find(".LineID").val());
        $("#TimeStart").val($(this).find(".TimeStart").html());
        $("#TimeEnd").val($(this).find(".TimeEnd").html());
        $("#Line1").val(htmlDecode($(this).find(".Line1").html()));
        $("#Line2").val(htmlDecode($(this).find(".Line2").html()));
    });

    //function to decode html
    function htmlDecode(input) {
        var e = document.createElement('div');
        e.innerHTML = input;
        return e.childNodes.length === 0 ? "" : e.childNodes[0].nodeValue;
    }

    //Update selected line
    $("#btnSave").click(function () {
        var Line = {
            LineID: $("#LineID").val(),
            TimeStart: $("#TimeStart").val(),
            TimeEnd: $("#TimeEnd").val(),
            Line1: $("#Line1").val(),
            Line2: $("#Line2").val(),
            SubtitleID: $("#SubtitleID").val()
        };

        $.ajax({
            type: "GET",
            url: "/Ajax/SaveLine",
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            data: Line,
            success: function (data) {
                var form = document.getElementById("EditForm").reset();
                $("#ErrorInputMessage").hide();
                var id = "#" + Line.LineID;
                $(id).find(".TimeStart").html(Line.TimeStart);
                $(id).find(".TimeEnd").html(Line.TimeEnd);
                $(id).find(".Line1").html(Line.Line1);
                $(id).find(".Line2").html(Line.Line2);
            },
            error: function () {
                $("#ErrorInputMessage").show();
                $('.form-group').addClass("has-error");
            }
        });
        
        return false;
    });
    //Add a new line
    $("#btnAdd").click(function () {
        var Line = {
            TimeStart: $("#TimeStart").val(),
            TimeEnd: $("#TimeEnd").val(),
            Line1: $("#Line1").val(),
            Line2: $("#Line2").val(),
            SubtitleID: $("#SubtitleID").val()
        };
        $.ajax({
            type: "GET",
            url: "/Ajax/AddLine",
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            data: Line,
            success: function (data) {
                var form = document.getElementById("EditForm").reset();
                $('.form-group').removeClass("has-error");
                $("#ErrorInputMessage").hide();
                //Refresh table

                var url = "/Subtitle/EditTablePartialView/" + $("#SubtitleID").val() + "?PageNumber=" + $("#PageNumber").val();
                $.get(url, function (data) {
                    $('#EditPageTable').html(data);
                });
            },
            error: function () {
                $("#ErrorInputMessage").show();
                $('.form-group').addClass("has-error");
            }
        });
        return false;
    });
    //delete selected line
    $("#btnDelete").click(function () {
        var Line = {
            LineID: $("#LineID").val(),
            TimeStart: $("#TimeStart").val(),
            TimeEnd: $("#TimeEnd").val(),
            Line1: $("#Line1").val(),
            Line2: $("#Line2").val(),
            SubtitleID: $("#SubtitleID").val()
        };
        $.ajax({
            type: "GET",
            url: "/Ajax/DeleteLine",
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            data: Line,
            success: function (data) {
                var form = document.getElementById("EditForm").reset();
                $('.form-group').removeClass("has-error");
                $("#ErrorMessage").hide();
                //Refresh table
                var url = "/Subtitle/EditTablePartialView/" + $("#SubtitleID").val() + "?PageNumber=" + $("#PageNumber").val();
                $.get(url, function (data) {
                    $('#EditPageTable').html(data);
                });
            },
            error: function () {
                //nothing should happen
            }
        });
        return false;
    });
});