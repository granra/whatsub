﻿$(document).ready(function () {
    var id = $("ul.list-group").data("id");

    // Check if there is needs for comments on the page
    if ($("#CommentList").length > 0)
    {
        $.ajax({
            type: "GET",
            contentType: "application/json; charset=utf8",
            url: "/Subtitle/GetComments/" + id,
            data: {},
            dataType: "json",
            success: function (data) {
                displaycomments(data);
            }
        });
    }

    /*
    * Issues when the submit comment button is pressed
    * Returns the content of the comment text box
    * and the subtitle id to the Subtitle Controller
    * via Ajax
    */
    $("#CommentForm").submit(function () {
        var commentText = $("#CommentText").val();
        if (commentText !== ""){
            var comment = {
                CommentText: commentText,
                SubtitleID: id
            };

            $.ajax({
                type: "POST",
                url: "/Subtitle/AddComment",
                data: comment,
                datatype: "json",
                success: function (data) {
                    $("#CommentText").val("");
                    displaycomments(data);
                }
            });
        }
        
        return false;
    });
    /*
    * Takes care of displaying the comments
    * with the use of ajax. Empties the list of 
    * comments, re creates it and sends it to the 
    * comment template
    */
    function displaycomments(commentdata) {
        $("#CommentList").empty();
        for (var i = 0; i < commentdata.length; i++) {
            var Date = moment(commentdata[i].CommentDate).fromNow();
            $('#CommentList').loadTemplate($('#CommentTemplate'), {
                Username: commentdata[i].Username,
                CommentDate: Date,
                CommentText: commentdata[i].CommentText
            }, { "append": true }
            );
        }
    };

});