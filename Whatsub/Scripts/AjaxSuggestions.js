﻿$(document).ready(function () {

    $(".WantSubtitleSuggestions").autocomplete({
        serviceUrl: '/Ajax/SubtitleSuggestion',
        minChars: 1
    });

    $(".WantRequestSuggestions").autocomplete({
        serviceUrl: '/Ajax/RequestSuggestion',
        minChars: 1
    });
});