﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Whatsub.Startup))]
namespace Whatsub
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
