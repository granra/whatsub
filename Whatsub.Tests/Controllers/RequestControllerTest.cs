﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using System.Data.Sql;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Whatsub;
using Whatsub.Controllers;
using Whatsub.Tests.Mock;
using Whatsub.Repositories;
using Whatsub.Models;

namespace Whatsub.Tests.Controllers
{
    [TestClass]
    public class RequestControllerTest
    {
        [TestMethod]
        public void TestIfIndexShowsMoreThan10Requests()
        {
            // Arrange
            List<SubtitleProfile> Profiles = new List<SubtitleProfile>();
            
            for (int i = 0; i < 15; ++i)
            {
                Profiles.Add(new SubtitleProfile
                {
                    ID = i,
                    Title = "Timecop " + i,
                    Year = 2000 + 1,
                    Requests = new List<SubtitleRequest>(),
                    MovieDBID = 1000,
                    TypeID = 1
                });
            }

            foreach (var Profile in Profiles)
            {
                Profile.Requests.Add(new SubtitleRequest
                {
                    ID = 1,
                    Language = new Language { ID = 1, LanguageName = "Icelandic" },
                    Upvotes = new List<Upvote>()
                });

                foreach (var Request in Profile.Requests)
                {
                    Request.Upvotes.Add(new Upvote());
                }
            }
            
            var MockRepo = new MockProfileRepository(Profiles);
            var Controller = new RequestController(MockRepo);

            // Act
            var result = Controller.Index(1);

            // Assert
            var ViewResult = (ViewResult)result;
            PageModel<RequestViewModel> model = ViewResult.Model as PageModel<RequestViewModel>;

            Assert.IsTrue(model.Profiles.Count == 10);
        }

        [TestMethod]
        public void TestIfRequestsAreSortedByUpvotes()
        {
            // Arrange
            List<SubtitleProfile> Profiles = new List<SubtitleProfile>();

            Random random = new Random();

            for (int i = 0; i < 15; ++i)
            {
                Profiles.Add(new SubtitleProfile
                {
                    ID = i,
                    Title = "Timecop " + i,
                    Year = 2000 + 1,
                    Requests = new List<SubtitleRequest>(),
                    MovieDBID = 1000,
                    TypeID = 1
                });
            }

            foreach (var Profile in Profiles)
            {
                Profile.Requests.Add(new SubtitleRequest
                {
                    ID = 1,
                    Language = new Language { ID = 1, LanguageName = "Icelandic" },
                    Upvotes = new List<Upvote>()
                });
            }

            for (int i = 0; i < 500; ++i)
            {
                Profiles[random.Next(0, 14)].Requests.First().Upvotes.Add(new Upvote());
            }

            var MockRepo = new MockProfileRepository(Profiles);
            var Controller = new RequestController(MockRepo);

            // Act
            var result = Controller.Index(1);

            // Assert
            var ViewResult = (ViewResult)result;
            PageModel<RequestViewModel> model = ViewResult.Model as PageModel<RequestViewModel>;
                // Check if it's ordered by upvotes, most upvotes first
            for (int i = 1; i < 10; ++i)
            {
                Assert.IsTrue(model.Profiles[i - 1].Upvotes >= model.Profiles[i].Upvotes);
            }
        }

        [TestMethod]
        public void TestSearch()
        {

        }
        [TestMethod]
        public void TestAdd()
        {

        }
    }
}
