﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Whatsub;
using Whatsub.Controllers;
using Whatsub.Tests.Mock;
using Whatsub.Repositories;
using Whatsub.Models;

namespace Whatsub.Tests.Controllers
{
    [TestClass]
    public class HomeControllerTest
    {
        [TestMethod]
        public void TestIndex()
        { 
            // Arrange
            List<SubtitleProfile> Profiles = new List<SubtitleProfile>();
            for (int i = 0; i < 10; ++i)
            {
                Profiles.Add(new SubtitleProfile
                {
                    ID = i,
                    Downloads = new List<Download>(),
                    Subtitles = new List<Subtitle>(),
                    Title = "Timecop " + i,
                    Year = 2000 + i
                });

                for (int r = 0; r < i; ++r)
                {
                    Profiles[i].Downloads.Add(new Download
                    {
                        ID = r,
                        SubtitleProfileID = Profiles[i].ID,
                        SubtitleProfile = Profiles[i]
                    });
                }

                for (int k = i; k > 0; --k)
                {
                    Profiles[i].Subtitles.Add(new Subtitle
                    {
                        ID = k,
                        LanguageID = k,
                        SubtitleProfileID = Profiles[i].ID
                    });
                }
            }

            IRepository<SubtitleProfile> MockRepo = new MockProfileRepository(Profiles);
            var Controller = new HomeController(MockRepo);

            // Act
            var Result = Controller.Index();

            // Assert
            var viewResult = (ViewResult)Result;
            IndexViewModel model = (viewResult.Model as IndexViewModel);
                // Check if both lists contain exactly 5 Subtitles
            Assert.IsTrue(model.NewSubtitles.Count() == 5);
            Assert.IsTrue(model.TopSubtitles.Count() == 5);
        }
    }
}
