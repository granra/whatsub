﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Whatsub;
using Whatsub.Controllers;
using Whatsub.Tests.Mock;
using Whatsub.Repositories;
using Whatsub.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Web.Mvc;

namespace Whatsub.Tests.Controllers
{
    [TestClass]
    public class SubtitleControllerTest
    {
        [TestMethod]
        public void TestSearch()
        {
            // Arrange
            List<SubtitleProfile> Profiles = new List<SubtitleProfile>();
            Profiles.Add(new SubtitleProfile
            {
                ID = 1,
                Title = "The Amazing Spider-Man",
                Subtitles = new List<Subtitle>(),
                MovieDBID = 1000,
                TypeID = 1
            });
            Profiles.Add(new SubtitleProfile
            {
                ID = 2,
                Title = "Elaine 2",
                Subtitles = new List<Subtitle>(),
                MovieDBID = 1000,
                TypeID = 1
            });
            Profiles.Add(new SubtitleProfile
            {
                ID = 3,
                Title = "The Matrix",
                Subtitles = new List<Subtitle>(),
                MovieDBID = 1000,
                TypeID = 1
            });

            foreach (var Profile in Profiles)
            {
                Profile.Subtitles.Add(new Subtitle());
            }

            IRepository<SubtitleProfile> MockRepo = new MockProfileRepository(Profiles);
            var Controller = new SubtitleController(MockRepo);

            // Act
            var Result = Controller.Search("The");

            // Assert
            var viewResult = (ViewResult)Result;
            PageModel<SubtitleSearchViewModel> SearchForThe = viewResult.Model as PageModel<SubtitleSearchViewModel>;
                // Only two profile names include the word "The"
            Assert.IsTrue(SearchForThe.Profiles.Count() == 2);
        }
    }
}
