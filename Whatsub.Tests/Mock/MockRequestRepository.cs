﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Threading.Tasks;
using Whatsub.Models;
using Whatsub.Repositories;

namespace Whatsub.Tests.Mock
{  
    public class MockRequestRepository : IRepository<SubtitleRequest>
    {
        private readonly List<SubtitleRequest> _Requests;
        public MockRequestRepository(List<SubtitleRequest> Requests)
        {
            _Requests = Requests;
        }

        public IQueryable<SubtitleRequest> GetAll()
        {
            return _Requests.AsQueryable();
        }
        public void Add(SubtitleRequest request)
        {
            _Requests.Add(request);

        }

        public void Delete(SubtitleRequest request)
        {
            _Requests.Remove(request);
        }

        public void Save()
        {
            //
        }
         
    } 
}
