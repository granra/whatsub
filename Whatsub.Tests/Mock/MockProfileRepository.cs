﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Whatsub.Models;
using Whatsub.Repositories;

namespace Whatsub.Tests.Mock
{
    class MockProfileRepository : IRepository<SubtitleProfile>
    {
        private readonly List<SubtitleProfile> _Profiles;
        public MockProfileRepository(List<SubtitleProfile> Profiles)
        {
            _Profiles = Profiles;
        }

        public IQueryable<SubtitleProfile> GetAll()
        {
            return _Profiles.AsQueryable();
        }

        public void Add(SubtitleProfile NewSubtitleProfile)
        {
            // TODO
        }

        public void Delete(SubtitleProfile SubtitleProfile)
        {
            // TODO
        }

        public void Save()
        {
            // _context.SaveChanges();
        }
    }
}
