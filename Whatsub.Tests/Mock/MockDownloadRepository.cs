﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Whatsub.Repositories;
using Whatsub.Models;

namespace Whatsub.Tests.Mock
{
    class MockDownloadRepository : IRepository<Download>
    {
        private readonly List<Download> _Downloads;
        public MockDownloadRepository(List<Download> Downloads)
        {
            _Downloads = Downloads;
        }

        public IQueryable<Download> GetAll()
        {
            return _Downloads.AsQueryable();
        }

        public void Add(Download NewDownload)
        {
            _Downloads.Add(NewDownload);
        }

        public void Delete(Download Download)
        {
            _Downloads.Remove(Download);
        }

        public void Save()
        {
            // _context.SaveChanges();
        }
    }
}
