﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Whatsub.Entity;
using Whatsub.Repositories;

namespace Whatsub.Tests.Mock
{
    class MockSubtitleRepository : IRepository<Subtitle>
    {
        private readonly List<Subtitle> _Subtitles;
        public MockSubtitleRepository(List<Subtitle> Subtitles)
        {
            _Subtitles = Subtitles;
        }

        public IQueryable<Subtitle> GetAll()
        {
            return _Subtitles.AsQueryable();
        }

        public void Add(Subtitle NewSubtitle)
        {
            _Subtitles.Add(NewSubtitle);
        }

        public void Delete(Subtitle Subtitle)
        {
            _Subtitles.Remove(Subtitle);
        }

        public void Save()
        {
            // _context.SaveChanges();
        }
    }
}
