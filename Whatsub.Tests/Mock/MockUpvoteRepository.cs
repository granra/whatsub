﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Threading.Tasks;
using Whatsub.Models;
using Whatsub.Repositories;

namespace Whatsub.Tests.Mock
{
    class MockUpvoteRepository : IRepository<Upvote>
    {
        private readonly List<Upvote> _Upvote;
        public MockUpvoteRepository(List<Upvote> Upvote)
        {
            _Upvote = Upvote;
        }

        public IQueryable<Upvote> GetAll()
        {
            return _Upvote.AsQueryable();
        }
        public void Add(Upvote upvote)
        {
            _Upvote.Add(upvote);

        }

        public void Delete(Upvote upvote)
        {
            _Upvote.Remove(upvote);
        }

        public void Save()
        {
            //
        }
    }
}
